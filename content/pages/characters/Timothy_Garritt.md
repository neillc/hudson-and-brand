Title: Timothy Garritt
Status: hidden




Timothy Garritt



|              |              |              |
|--------------|--------------|--------------|
| STR 35/17/07 | DEX 45/22/09 | INT 90/45/18 |
| CON 65/32/13 | APP 30/15/06 | POW 80/40/16 |
| SIZ 50/25/10 | EDU 67/33/13 | MOV 7        |

Appraise 05/02/01, Archaeology 03/01/00, Art/Craft - Technical Drawing 50/25/10, Charm 15/07/03, Credit Rating 60/30/12, Disguise 05/02/01, Dodge 22/11/04, Drive Carriage 20/10/04, Fighting - Brawl 25/12/05, Firearms - Handgun 45/22/09, Firearms - Rifle/Shotgun 35/17/07, First Aid 30/15/06, History 05/02/01, Intimidate 20/10/04, Jump 20/10/04, Language (Other) - French 20/10/04, Language (Own) - English 65/32/13, Law 15/07/03, Library Use 35/17/07, Locksmith 31/15/06, Mechanical Repair 25/12/05, Navigate 25/12/05, Occult 05/02/01, Operate Heavy Machine 25/12/05, Persuade 15/07/03, Ride 35/17/07, Science - Engineering 60/30/12, Science - Physics 30/15/06, Spot Hidden 35/17/07, Stealth 25/12/05, Misc - Demolitions 36/18/07

###Weapons
| Name | % | Dmg | Range |Attacks | Malf |
|--------------|--------------|--------------|---|---|---|
| Unarmed Brawl | 25/12/05 | 1d4+db | - | 1 | - | - || .30-06 Bolt-Action Rifle Rifle | 35/17/07 | 2D6+4 | 110 yards | 1 | 5 | 100 |
