Title: Gordon Bennet
Status: hidden




Gordon Bennet



|              |              |              |
|--------------|--------------|--------------|
| STR 80/40/16 | DEX 50/25/10 | INT 60/30/12 |
| CON 70/35/14 | APP 50/25/10 | POW 50/25/10 |
| SIZ 60/30/12 | EDU 40/20/08 | MOV 8        |

Appraise 05/02/01, Archaeology 01/00/00, Art/Craft - Furniture 20/10/04, Charm 15/07/03, Credit Rating 50/25/10, Disguise 05/02/01, Dodge 25/12/05, Drive Carriage 20/10/04, Fighting - Brawl 40/20/08, Firearms - Handgun 20/10/04, Firearms - Rifle/Shotgun 70/35/14, First Aid 50/25/10, History 05/02/01, Intimidate 50/25/10, Jump 20/10/04, Language (Other) - Pushtan 40/20/08, Language (Own) - English 40/20/08, Law 05/02/01, Library Use 20/10/04, Listen 60/30/12, Occult 05/02/01, Persuade 30/15/06, Ride 50/25/10, Spot Hidden 60/30/12, Stealth 40/20/08, Survival - Afghanistan 50/25/10, Misc - Demolitions 36/18/07

###Weapons
| Name | % | Dmg | Range |Attacks | Malf |
|--------------|--------------|--------------|---|---|---|
| Unarmed Brawl | 40/20/08 | 1d3+db | - | 1 | - | - || .30-06 Bolt-Action Rifle Rifle | 70/35/14 | 2D6+4 | 110 yards | 1 | 5 | 100 |
