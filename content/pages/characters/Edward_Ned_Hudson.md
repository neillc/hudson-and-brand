Title: Edward 'Ned' Hudson
Status: hidden




Edward 'Ned' Hudson



|              |              |              |
|--------------|--------------|--------------|
| STR 70/35/14 | DEX 65/32/13 | INT 60/30/12 |
| CON 65/32/13 | APP 45/22/09 | POW 45/22/09 |
| SIZ 65/32/13 | EDU 50/25/10 | MOV 8        |

Appraise 15/07/03, Archaeology 01/00/00, Art/Craft - Gardener 51/25/10, Charm 15/07/03, Climb 35/17/07, Credit Rating 14/07/02, Disguise 05/02/01, Dodge 32/16/06, Drive Carriage 45/22/09, Fighting - Brawl 45/22/09, Firearms - Handgun 20/10/04, Firearms - Rifle/Shotgun 55/27/11, First Aid 45/22/09, History 05/02/01, Jump 20/10/04, Language (Other) - Pashtun 26/13/05, Language (Own) - English 45/22/09, Law 05/02/01, Library Use 20/10/04, Listen 35/17/07, Mechanical Repair 30/15/06, Occult 05/02/01, Psychology 20/10/04, Ride 15/07/03, Spot Hidden 50/25/10, Stealth 30/15/06, Swim 25/12/05, Track 20/10/04

###Weapons
| Name | % | Dmg | Range |Attacks | Malf |
|--------------|--------------|--------------|---|---|---|
| Unarmed Brawl | 45/22/09 | 1d4+db | - | 1 | - | - |
