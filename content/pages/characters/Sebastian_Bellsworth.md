Title: Sebastian Bellsworth
Status: hidden




Sebastian Bellsworth



|              |              |              |
|--------------|--------------|--------------|
| STR 48/24/09 | DEX 64/32/12 | INT 75/37/15 |
| CON 48/24/09 | APP 50/25/10 | POW 40/20/08 |
| SIZ 55/27/11 | EDU 71/35/14 | MOV 7        |

Appraise 05/02/01, Archaeology 01/00/00, Charm 50/25/10, Credit Rating 35/17/07, Disguise 05/02/01, Dodge 32/16/06, Drive Carriage 25/12/05, Fast Talk 15/07/03, Fighting - Brawl 25/12/05, Firearms - Handgun 45/22/09, Firearms - Rifle/Shotgun 30/15/06, First Aid 65/32/13, History 05/02/01, Jump 20/10/04, Language (Other) - Latin 71/35/14, Language (Own) - English 70/35/14, Law 30/15/06, Library Use 35/17/07, Medicine 71/35/14, Occult 05/02/01, Persuade 50/25/10, Psychology 25/12/05, Science - Biology 25/12/05, Science - Pharmacy 71/35/14, Spot Hidden 25/12/05

###Weapons
| Name | % | Dmg | Range |Attacks | Malf |
|--------------|--------------|--------------|---|---|---|
| Unarmed Brawl | 25/12/05 | 1d4+db | - | 1 | - | - |
