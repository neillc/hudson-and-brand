Title: Hubert Grange
Status: hidden




Hubert Grange



|              |              |              |
|--------------|--------------|--------------|
| STR 42/21/08 | DEX 45/22/09 | INT 80/40/16 |
| CON 43/21/08 | APP 40/20/08 | POW 60/30/12 |
| SIZ 50/25/10 | EDU 81/40/16 | MOV 5        |

Appraise 65/32/13, Archaeology 50/25/10, Charm 15/07/03, Credit Rating 15/07/03, Disguise 05/02/01, Dodge 25/12/05, Drive Carriage 20/10/04, Fighting - Brawl 25/12/05, Firearms - Handgun 20/10/04, Firearms - Rifle/Shotgun 25/12/05, First Aid 30/15/06, History 70/35/14, Jump 20/10/04, Language (Other) - Latin 75/37/15, Language (Own) - English 80/40/16, Library Use 75/37/15, Science - Botany 60/30/12, Science - Entymology 51/25/10, Science - Zoology 32/16/06, Spot Hidden 26/13/05

###Weapons
| Name | % | Dmg | Range |Attacks | Malf |
|--------------|--------------|--------------|---|---|---|
| Unarmed Brawl | 25/12/05 | 1d4+db | - | 1 | - | - |
