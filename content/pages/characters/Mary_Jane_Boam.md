Title: Mary Jane Boam
Status: hidden




Mary Jane Boam



|              |              |              |
|--------------|--------------|--------------|
| STR 55/27/11 | DEX 55/27/11 | INT 80/40/16 |
| CON 50/25/10 | APP 65/32/13 | POW 55/27/11 |
| SIZ 45/22/09 | EDU 59/29/11 | MOV 9        |

Anthropology 11/05/02, Appraise 20/10/04, Archaeology 11/05/02, Art/Craft - Acting 35/17/07, Charm 45/22/09, Credit Rating 30/15/06, Disguise 25/12/05, Dodge 27/13/05, Drive Carriage 20/10/04, Fighting - Brawl 25/12/05, Firearms - Handgun 65/32/13, Firearms - Rifle/Shotgun 25/12/05, First Aid 30/15/06, History 15/07/03, Jump 20/10/04, Language (Own) - English (American) 75/37/15, Law 20/10/04, Library Use 55/27/11, Listen 50/25/10, Natural World 20/10/04, Occult 05/02/01, Psychology 30/15/06, Spot Hidden 60/30/12, Stealth 55/27/11

###Weapons
| Name | % | Dmg | Range |Attacks | Malf |
|--------------|--------------|--------------|---|---|---|
| Unarmed Brawl | 25/12/05 | 1d4+db | - | 1 | - | - |
