Title: Session One 
Date: 2018-01-21 10:00 
Category: Hudson and Brand

Hudson and Brand - 33 Golden Square, Soho 21 May 1889

##Dramatis Personae: 
- Gordon Bennet: Late of Loyal North Lancashire Regiment, current proprietor of Hudson and Brand.
- Timothy Garrit: A clumsy demolitions expert, friend of Gordon Bennet.
- Sebastian: A doctor who doesn't like treating patients.
- Hubert Grange: Acquisitions expert and curator for the London Museum of Natural History.
- Mary Jane Boam: Budding investigative journalist, currently investigating fashion and society.
- Ned Hudson: Gordon's batman. Prefers dogs to people. Prefers plants to people. Not keen on people really.
- James Markham: Butler extroardinaire. Uncanny ability to anticipate his employers' needs.

Gordon Bennet, recently a Lieutenant in the British Army, has inherited a home
and consulting detective agency from his uncle Albert Hudson. Unfortunately he has not
inherited the means to support said home and agency for more than the next
six months or so. Even more unfortunately he has no idea how to actually run a
detective agency. Seeking a way forward, he invites some of his army friends,
one of his Uncle's acquaintances and Mary Jane to dinner.

Mary Jane Boam is Ulysses Brand's cousin. Unusually, she is also a journalist, 
a profession she shares with her cousin. Ulysses had actually encouraged her to
pursue her passion and been helpful in getting her started. Mary has convinced
her editor that what the New York Journal really needed was a fashion and
society correspondent in London, the heart of the empire. She was quite
distressed to discover that her cousin has disappeared and is presumed dead.

Gordon, Timothy, Sebastian and Ned all know each other from their time in
Afghanistan. While they (mostly) did not serve in the same units, they all came
to know each other professionally and socially.  

Gordon has a problem. He has inherited a consulting detective agency and
sufficient funds to run it for about six months, but he knows nothing about
being a detective. Frankly, his plan was to marry a rich (hopefully young,
hopefully pretty) woman to support his desired lifestyle. Mary Jane was briefly
of interest, but he quickly determined her unsuitability (she's not rich).
She's also a bit too shrewd to succumb to Gordon's charms.  

While Gordon and his friends are discussing how he might earn a living as a
consulting detective, Markham shows Albie into the room.

Albie is looking for help in finding his friend "Bare knuckle" Bill, a pugilist
who recently lost a fight, angering his patrons (an Irish gang known as the
Greens).

The investigators learn that:
  - Albie has no money, but he's heard that the 'tectives often help people.
  - Bill was not expected to lose the fight.
  - The Greens were very angry that he lost.
  - Some Greens came and collected Bill after the fight and took him back to the Coopers yard.
  - No one has seen Bill since.
  - Albie overheard some Greens saying:
      "That Bill fella had been a tough one to put down last night."

At this point fatigue and emotion overcomes Albie and he breaks down. Albie is
sent to rest while the investigators discussed their next steps.

Gordon is reluctant to take the case given he was running a detective agency
rather than a charity but the others convince him that it would be good
publicity and a way to start to learn the business.

After much discussion it is decided that it is too late in the evening to do
anything and instead the investigators will reconvene in the morning to start
looking into the case.
