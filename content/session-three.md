Title: Session Two
Date: 2018-02-17 12:00
Category: Hudson and Brand
 
Coopers Yard and tunnels beneath, 23rd May 1889

##Dramatis Personae: 
- Gordon Bennet: Late of Loyal North Lancashire Regiment, current proprietor of Hudson and Brand
- Timothy Garrit: A clumsy demolitions expert, friend of Gordon Bennet
- Sebastian : A doctor who doesn't like treating patients
- Hubert Grange: Acquisitions expert and curator for the London Museum of Natural History
- Mary Jane Boam: Budding investigative journalist, currently investigating fashion and society.
- Ned Hudson: Gordon's batman. Prefers dogs to people. Prefers plants to people. Not keen on people really.
- A tiger?

The investigators decide to search the coopers' yard to find either Bare-knuckle
Bill or clues to his current whereabouts.

They decide to make their entry via the side door in the alley. Timothy picks
the lock and they find a small entry vestibule. Mary declaring that she is very
good at finding hidden things leads the way.

This boast seems accurate, as Mary immediately notices that some of the floor
tiles in front of the doorway are a different colour, and on examination seems
loose and slightly flimsy. The investigators carefully avoid these tiles.

Exploring the building they find no evidence of current occupants, but do find
the boxing ring and what look like offices, before finding the boxers room at
the back of the building.

Exploring that room they discover a trap door leading down into a dark tunnel.
Equipping themselves with oil and lanterns they descend into the darkness.

The tunnel is well built, finished with brick and seems of modern construction,
but not particularly recent. It also smells unpleasant. A mixture of shit and
spoiled meat.

After 150 metres or so the tunnel ends in a tee junction at the sewers. There
being no obvious difference in either direction they head right. Scattered
along the floor of the tunnel they find pieces of bone, sometimes with scraps
of flesh still attached, rags, fur and scat, which Gordon finds strongly
reminiscent of tiger.

After following the sewers for a considerable distance the investigators come
to steps up into an antechamber. The door on the far side of the antechamber
opens in to what seems to be some sort of labyrinth. With some trepidation the
group enters.

Following the twists and turns of the maze they are attacked by a tiger. A huge
Bengal tiger, lunges out of the darkness mauling  Mary (who because of her
skill at finding hidden things is leading the way) and knocking her to the
ground and following up with an attack on Gordon. What seems like an eternity
of confusion, fear and pain follows as the investigators battle the tiger in
the cramped setting of its lair, but eventually they manage to kill it.

After this battle Sebastian's medical skills are put to the test as he patches,
bandages, cleans and stitches. Mary unfortunately, is likely to bear the scars
of her encounter with the tiger for the rest of her life.

Much the worse for wear the investigators continue through the maze to see what
is on the other side.

As they leave the tiger's lair, Mary makes an horrific discovery. In the lair
is a partially eaten body that she recognizes as her cousin Ulysses. Mary is
severely shaken by this discovery and vows to have no mercy on the remaining
Greens.

Past the tiger's lair they discover another locked door. Timothy again
employs his locksmith's skills and manages to unlock the door. Beyond the door
they find a wine cellar and stairs leading upwards. What will they find on the
at the top of the stairs?





