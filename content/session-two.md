Title: Session Two
Date: 2018-01-21 12:00
Category: Hudson and Brand

Hudson and Brand - 33 Golden Square, Soho, London 22 May 1889

##Dramatis Personae: 
- Gordon Bennet: Late of Loyal North Lancashire Regiment, current proprietor of Hudson and Brand.
- Timothy Garrit: A clumsy demolitions expert, friend of Gordon Bennet.
- Sebastian: A doctor who doesn't like treating patients.
- Hubert Grange: Acquisitions expert and curator for the London Museum of Natural History.
- Mary Jane Boam: Budding investigative journalist, currently investigating fashion and society.
- Ned Hudson: Gordon's batman. Prefers dogs to people. Prefers plants to people. Not keen on people really.
- James Markham: Butler extroardinaire. Uncanny ability to anticipate his employers' needs.
- Sara Levi: Cook and housekeeper. Strong believer in the restorative properties of Gin.
- Sergeant William Parlow: Former colleague of Hudson and Brand.
- Albie: Young rapscallion. Friend of "Bare knuckle" Bill.
- Various Greens, washerwomen, bar patrons, urchins and coopers' labourers

The investigators re-convene over breakfast to discuss their next steps.

They decide to pursue a number of avenues, starting with visiting Bill's
lodgings and following up with a visit to the cooperage and Bill's local, the
Elephant and Castle.

Stopping at Bill's lodgings they meet Leonard, the manager of the premises. Mary
Jane's American charms fail to convince him to allow the investigators access
to Bill's rooms, but he does agree to have a look inside himself after some
persuasion from Gordon.

The investigators wait in the parlour while Leonard goes upstairs for a look.

When Leonard returns he reports nothing out of the ordinary. In a general
conversation about Bill he expresses satisfaction with him as a lodger, noting
that while he was occasionally a bit late with the rent he always paid
eventually. Asked when Bill's rent was next due he said that it was paid for
the next week and a half.

Leonard suggests a visit to the Elephant and Castle if the investigators
want to talk to other people who knew Bill.

Next stop is the cooperage yard. It is locked up, quiet and dark. Mary and 
Hubert have a bit of a stroll around the yard to see if they can look at the 
rear of the warehouse, but there is no ready access. They do discover a small 
side gate which is securely locked and a rear gate that has clearly not been 
used for a long time.

However, the investigators' presence is noticed, and a couple of fairly rough
looking men lounge at the inner doors. Gordon recognizes them as members of
the Greens from his visits to the boxing matches run here.

When approached they also deny any knowledge of Bill's whereabouts.

Following this fruitless inquiry the investigators decide to visit one of
Gordon's gambling friends, Giles, at his club.

Giles was actually at the fight and gives a description of the evening.

According to Giles, Bill's opponent on the night was Wat Tyler (note to self -
make sure to have some names handy). Wat is an experienced pugilist but was
definitely not expected to beat Bill. The odds were 7:4 in Bill's favour. The
fight was not short (going for eight rounds until Bill was knocked to the
ground and unable to continue). Bill did not seem to be drunk or drugged,
nor did Wat seem unusually quick or strong. In Giles' opinion the fight seemed
genuine. Nevertheless the Greens were definitely very upset over the result and
Giles thinks they must have lost significant amounts of money over the match.

After talking to Giles the investigators decide their next stop should now
be the Elephant and Castle to speak to the patrons there.

On there way they saw a group of washerwomen, and some children playing in the
street and decide to approach them to ask if they know anything about Bill.

Mary speaks to the washerwomen who are amazed that Mary is a working reporter.
They ask about how her husband feels about her working and when they
discover she is single they are astonished that her father had permitted
her to travel from America to London unaccompanied. Still, they do confide to
Mary that the cooperage had been locked up tight after the fight but loud voices
could be heard and the lights were lit well into the night, something they had
never seen happen before.

The children confirmed that the fight had ended early and that angry shouts
could be heard coming from the warehouse.

Dr Sebastian is concerned that Gordon is very agitated and distressed by the
investigation and gives him a tonic to calm his nerves. 

Proceeding to the pub, they enter, but Mary is refused entry to the public
bar and instead directed to the lady's parlour.

Fortified with the tonic, Gordon proceeds to buy rounds of drinks for all of
his friends as they speak to the locals inside the pub. This uncharacteristic
generosity causes considerable concern among Gordon's friends and the general
conclusion is the Sebastian had overdone the tonic.

Mary, having been excluded from the public bar by her gender, decides to do some
investigating on her own. She spots the obvious tail who ducks into an alley,
but fails badly when the second thug sneaks up on her with a chloroform pad.

The main party spends some time and money drinking in the public bar, but
learns little beyond that Bill was very popular with the locals and nobody
knows where he might be. After some time they decide to collect Mary from the
lady's parlour only to discover that she is not there.

Discovering that Mary is missing causes Gordon significant anxiety, so Sebastian
offers him another tonic to calm his nerves. Unfortunately there seems to have
been an error in the formulation of this second tonic, and Gordon
experiences an unfortunate and extreme reaction.

Screaming incoherently about spiders on and under his skin Gordon tears his
clothes off and runs down the street clad only in his smalls.

This leads in short order to his arrest by the police, who detain him and
deliver him to the nearest station. Gordon's faithful batman does his best to
collect his clothes and explain the situation to the officers, who are
particularly interested in Sebastian's medical credentials but not terribly
sympathetic to Gordon's plight. There are after all, women and children in the
area.

While Gordon is being detained Mary wakes. She has a hood over her head but
gets the impression that she is in a small room, with at least two other people
who seem to be playing some sort of card game. One of them is complaining that
the other seems far too lucky. They are having their increasingly agitated
discussion in English, but with distinct Irish accents.

Sometime after she awakes a third person enters and asks the first two if "she"
is awake yet. When they tell him no, he suggests slapping Mary around a bit to
wake her up. Mary decides at this point to let out a small, lady like groan.

The new person proceeds to warn Mary off, suggesting that she and
her friends should leave off their poking around.

After receiving this warning Mary is drugged again and lapses back into
unconsciousness.

The other investigators decide that the best course of action is to leave Gordon
in police lockup, though Gordon does ask for his solicitor to be contacted.

Sebastian, Ned, Timothy and Hubert regroup at the Elephant and Castle.

Gordon is eventually bailed out but told he will need to face a magistrate
later.

Mary recovers consciousness in an alley and heads back to Hudson and Brand,
arriving shortly after Gordon.

Gordon instructs Markham to draw a bath and find clean clothes for Mary.
Markham tells him this has already been done (how did he know?)

After Mary's bath, she and Gordon take tea and scones (and possibly gin for Mary,
on Mrs Levis' recommendation - she is a great believer in the restorative
properties of gin).

The rest of the party arrive home while Mary and Gordon are taking tea and
are ushered into the parlour.

While the investigators compare notes Sergeant Parlow arrives. Gordon is somewhat
displeased that Markham has shown a policeman in via the front door but Markham
explains that he was a friend of his uncle's.

Sergeant Parlow discusses his past involvement with Hudson and Brand - he seems
initially concerned that Gordon might have been drugged by an old enemy of
Hudson and Brand, but accepts that it was just an unfortunate reaction to one
of Sebastian's tonics.

The discussion turns to the Greens: what do the police know and what help might
they provide.

	- The cooperage is the only known stronghold of the Greens, but the gang
	  members do not live there.

	- O'Riordan and Seems in particular have lodgings at some other, unknown
	  location.

	- Without evidence the police cannot raid the cooperage.

	- If a fight was to start before evidence has been found they would have to
	  intervene, but probably to arrest the trespassers.

	- Should a party of private individuals find evidence of serious wrongdoing
	  and call for help Sergeant Parlow suggests that a patrol might well just
      happen to be in the vicinity and be able to respond quickly to calls for help.

Sergeant Parlow takes his leave, suggesting that if they think Bill is being held
within the cooperage then time is probably of the essence. Moving quickly
would be best, perhaps even tonight. 


